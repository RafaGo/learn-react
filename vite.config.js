import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react' 

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  extensions: ['.tsx', '.ts', '.jsx', '.js'], // Adicione as extensões que deseja suportar
})
