import React from "react"
export function Inativo() {

  const luana = {
    cliente: 'luana',
    idade: 27,
    comprar: [
      { nome: 'notebook', preco: '2500' },
      { nome: 'Geladeira', preco: '4000' },
      { nome: 'Tv', preco: '2000' }
    ],
    ativa: true,
  };

  const marcio = {
    cliente: 'Marcio',
    idade: 31,
    compras: [
      { nome: 'Notebook', preco: '2500' },
      { nome: 'Geladeira', preco: '4000' },
      { nome: 'Tv', preco: '2000' },
      { nome: 'Guitarra', preco: '5000' }

    ],
    ativa: false,
  };

  const App = () => {
    const dados = marcio;

    const total = dados.compras
      .map((item) => Number(item.preco)).reduce((a, b) => a + b)

    return (
      <div>
        <p>Nome : {dados.cliente}</p>
        <p>Idade : {dados.idade}</p>
        <p>
          Situação :
          <span style={{ color: dados.ativa ? 'green' : 'red' }}>
            {dados.ativa ? 'Ativa' : 'Inativa'}
          </span>
        </p>

        <p>Total : {total} </p>
        {total > 1000 && <p> Você está gastando muito</p>}
      </div>

    )

  }
}

export default Inativo
