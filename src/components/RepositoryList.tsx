import React from "react";
import { useEffect, useState } from "react";
import { RepostiroryItem } from "./RepositoryItem";

interface Repository {
  name: string;
  description: string;
  html_url: string;

}

export function RepostiroryList() {

  const [repositories, setRepositores] = useState<Repository[]>([])


  useEffect(() => {
    fetch('https://api.github.com/orgs/rocketseat/repos')
      .then(response => response.json())
      .then(data => setRepositores(data))
  }, [])

  console.log(repositories)

  return (
    <section className="repository-list">
      <h1>Lista de Repositorios</h1>
      <ul>
        {repositories.map((repository) => {
          return <RepostiroryItem key={repository.name} repository={repository} />
        })}
      </ul>

    </section>
  )
}